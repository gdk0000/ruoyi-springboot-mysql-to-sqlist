package com.ruoyi.web.controller.system;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * 充电桩
 *
 * @author gdk
 */

@Controller
@RequestMapping("/system/evcss")
public class SysEVCSSController {
    private String prefix = "system/evcss";



    @RequiresPermissions("system:evcss:view")
    @GetMapping()
    public String user()
    {
        return prefix + "/index";
    }
}
